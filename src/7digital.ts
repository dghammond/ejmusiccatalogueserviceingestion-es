import * as ES from '@elastic/elasticsearch'
import { ChunkCallback, parseCSVtoNDJSONinChunks, readNDJSONinChunks } from './parsers';
import { PromiseRingAwaiter } from './utils/promise';

const es = new ES.Client({ node: process.env.ES_NODE });

async function run_ingest(
  index: string,
  createIndex: (index: string) => Promise<void>,
  readFunction: (filename: string, chunksize: number, callback: ChunkCallback) => Promise<void>,
  filename: string,
  chunksize: number = 20000,
  requestConcurrency: number = 10
) {
  try {
    await es.indices.delete({ index });
    console.log('index', index, 'removed');
  } catch (err) {
    // ignore
    console.log('index', index, 'did not need to be removed');
  }
  await createIndex(index);
  console.log('index', index, 'created');

  const timing = {
    start: Date.now(),
    records: 0
  };

  const promiseBuffer: PromiseRingAwaiter<ES.ApiResponse<any, any>> = new PromiseRingAwaiter(requestConcurrency);

  const ingestChunk = async (chunk: any[], number: number) => {
    const body = chunk.map(c => `{"index":{"_index":"${index}"}}\n${c}`).join('\n') + '\n';
    // wrap the es request and swallow errors, and put it in the request buffer
    await promiseBuffer.push(new Promise(async (resolve, _reject) => {
      try {
        await es.bulk({ index, body });
        // TODO: can extract stats from the bulk response
      } catch (error) {
        console.log('es bulk request number', number, 'error:', error.message, '\n', error.meta.body);
      }
      resolve();
    }));
    timing.records += chunk.length;
    const lps = timing.records / ((Date.now() - timing.start) / 1000.0);
    console.log(`${filename}; got chunk ${number}; length ${chunk.length}; total records ${timing.records}; ${lps.toFixed(2)} records/sec`);
  };

  await readFunction(filename, chunksize, ingestChunk);
  // wait for all remaining requests
  await promiseBuffer.flush();
}

const LANGUAGE_SUBFIELDS = {
  'fields': {
    'fuzzy': {
      'type': 'text',
      'analyzer': 'simple'
    },
    'english': {
      'type': 'text',
      'analyzer': 'english'
    },
    'german': {
      'type': 'text',
      'analyzer': 'german'
    }
  }
};

const TAG_SUBFIELDS = {
  'fields': {
    'fuzzy': {
      'type': 'text',
      'analyzer': 'simple'
    },
    'tag': {
      'type': 'text',
      'analyzer': 'english'
    }
  }
};

async function createArtistIndex(index: string) {
  await es.indices.create({
    index,
    body: {
      'mappings': {
        'properties': {
          'searchvalue': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS
          },
          'artistId': {
            'type': 'integer'
          },
          'image': {
            'type': 'text'
          },
          'isPlaceholderImage': {
            'type': 'text'
          },
          'name': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'popularity': {
            'type': 'float'
          },
          'tags': {
            'type': 'text',
            ...TAG_SUBFIELDS
          },
          'url': {
            'type': 'text'
          }
        }
      }
    }
  });
}

async function createReleaseIndex(index: string) {
  await es.indices.create({
    index,
    body: {
      'mappings': {
        'properties': {
          // unified search field; other use "copy_to" to populate this
          'searchvalue': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS
          },

          'popularity': {
            'type': 'float'
          },
          'CLine': {
            'type': 'text'
          },
          'GRID': {
            'type': 'text'
          },
          'Popularity': {
            'type': 'float',
            'copy_to': 'popularity'
          },
          'VersionReleaseDate': {
            'type': 'date'
          },
          'artistAppearsAs': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'artistId': {
            'type': 'integer'
          },
          'barcode': {
            'type': 'text'
          },
          'dateAdded': {
            'type': 'date'
          },
          'duration': {
            'type': 'float'
          },
          'explicitContent': {
            'type': 'text'
          },
          'formats': {
            'type': 'text'
          },
          'image': {
            'type': 'text'
          },
          'labelId': {
            'type': 'integer'
          },
          'labelName': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS
          },
          'licensorId': {
            'type': 'integer'
          },
          'price': {
            'type': 'float'
          },
          'releaseDate': {
            'type': 'date',
            'ignore_malformed': true
          },
          'releaseId': {
            'type': 'integer'
          },
          'rrp': {
            'type': 'float'
          },
          'tags': {
            'type': 'text',
            ...TAG_SUBFIELDS
          },
          'title': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'trackCount': {
            'type': 'integer'
          },
          'type': {
            'type': 'text'
          },
          'url': {
            'type': 'text'
          },
          'version': {
            'type': 'text'
          },
          'year': {
            'type': 'integer'
          }
        }
      }
    }
  });
}

async function createTrackIndex(index: string) {
  await es.indices.create({
    index,
    body: {
      'mappings': {
        'properties': {
          // unified search field; other use "copy_to" to populate this
          'searchvalue': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS
          },
          'GRID': {
            'type': 'text'
          },
          'PLine': {
            'type': 'text'
          },
          'adSupportedReleaseDate': {
            'type': 'date',
            'ignore_malformed': true
          },
          'artistAppearsAs': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'artistId': {
            'type': 'integer'
          },
          'discNumber': {
            'type': 'integer'
          },
          'duration': {
            'type': 'float'
          },
          'explicitContent': {
            'type': 'text'
          },
          'formats': {
            'type': 'text'
          },
          'isrc': {
            'type': 'text'
          },
          'licensorId': {
            'type': 'integer'
          },
          'popularity': {
            'type': 'float'
          },
          'price': {
            'type': 'float'
          },
          'releaseId': {
            'type': 'integer'
          },
          'rrp': {
            'type': 'float'
          },
          'streamingReleaseDate': {
            'type': 'date',
            'ignore_malformed': true
          },
          'title': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'trackId': {
            'type': 'integer'
          },
          'trackNumber': {
            'type': 'integer'
          },
          'type': {
            'type': 'text'
          },
          'url': {
            'type': 'text'
          },
          'version': {
            'type': 'text'
          }
        }
      }
    }
  });
}

async function createLyricFindIndex(index: string) {
  await es.indices.create({
    index,
    body: {
      'mappings': {
        'properties': {
          // metadata record propeties
          // TODO: skip this record
          'date': {
            'type': 'date'
          },
          'file_id': {
            'type': 'text'
          },

          // unified search field; other use "copy_to" to populate this
          'searchvalue': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS
          },

          // lyric record properties
          'artists': {
            'properties': {
              'is_primary': {
                'type': 'boolean'
              },
              'name': {
                'type': 'text',
                ...LANGUAGE_SUBFIELDS,
                'copy_to': 'searchvalue'
              }
            }
          },
          'duration_in_seconds': {
            'type': 'long'
          },
          'is_instrumental': {
            'type': 'boolean'
          },
          'lfid': {
            'type': 'text'
          },
          'lyric': {
            'properties': {
              'language': {
                'type': 'text'
              },
              'lrc': {
                'properties': {
                  'duration': {
                    'type': 'integer'
                  },
                  'line': {
                    'type': 'text',
                    ...LANGUAGE_SUBFIELDS
                  },
                  'lrc_timestamp': {
                    'type': 'text'
                  },
                  'milliseconds': {
                    'type': 'integer'
                  }
                }
              },
              'lyric': {
                'type': 'text',
                ...LANGUAGE_SUBFIELDS,
                'copy_to': 'searchvalue'
              },
              'verified_lrc_state': {
                'type': 'long'
              },
              'verified_lyric_state': {
                'type': 'long'
              }
            }
          },
          'ownership': {
            'properties': {
              'hfa_code': {
                'type': 'text'
              },
              'iswc': {
                'type': 'text'
              },
              'territories': {
                'properties': {
                  'clearance': {
                    'type': 'text'
                  },
                  'publisher_credit': {
                    'type': 'text'
                  },
                  'territory': {
                    'type': 'text'
                  }
                }
              }
            }
          },
          'title': {
            'type': 'text',
            ...LANGUAGE_SUBFIELDS,
            'copy_to': 'searchvalue'
          },
          'version': {
            'type': 'text'
          },
          'writers': {
            'type': 'text'
          }
        }
      }
    }
  });
}

const ARTIST_FULL = '/data/ingest-files/7digital/local/20190909-1769-http___feeds.api.7digital.com_1.2_feed_artist_full';
const RELEASE_FULL = '/data/ingest-files/7digital/local/20190909-1769-http___feeds.api.7digital.com_1.2_feed_release_full';
const TRACK_FULL = '/data/ingest-files/7digital/local/20190909-1769-http___feeds.api.7digital.com_1.2_feed_track_full';
const LYRICS_FULL = '/data/ingest-files/7digital/local/ftp.lyricfind.com-lrc.json.gz';

Promise.resolve().then(
  async () => {
    // Be careful with chunksize; you can use larger values if the record size in a file is small,
    // but be aware that at some point elasticsearch may reject a bulk request if it becomes too large.
    // Therefore: artist data (small records) can use large chunks, lyrics data (large records) must use small chunks.
    // Also bear in mind that the greater the product (chunksize * concurrency) the more memory this script will require to run.

    // TODO: licensor filtering

    await run_ingest('catalogue-7digital-artist', createArtistIndex, parseCSVtoNDJSONinChunks, ARTIST_FULL, 50000);
    await run_ingest('catalogue-7digital-release', createReleaseIndex, parseCSVtoNDJSONinChunks, RELEASE_FULL);
    await run_ingest('catalogue-7digital-track', createTrackIndex, parseCSVtoNDJSONinChunks, TRACK_FULL);
    await run_ingest('catalogue-lyricfind-lyrics', createLyricFindIndex, readNDJSONinChunks, LYRICS_FULL, 800, 8);
    // TODO: processing of lfid-isrc mapping

    // TODO: processing of deltas

    console.info('Done.');
    process.exit(0);
  }
).catch(
  error => {
    console.error('Error:', error);
    process.exit(1);
  }
);

// Hurrah! error tolerant search !
//
// GET catalogue-*/_search
// {
//   "size": 50,
//   "sort": [
//     {
//       "popularity": {
//         "order": "desc"
//       }
//     }
//   ],
// "query": {
//   "bool": {
//     "must": [
//       {
//         "match": {
//           "searchvalue": {
//             "query": "kigs on lion",
//             "fuzziness": 2,
//             "lenient": "true",
//             "operator": "and"
//           }
//         }
//       }
//     ]
//   }
// }
// }

// GET catalogue-track/_search
// {
//   "size": 50,
//   "sort": [
//     {
//       "popularity": {
//         "order": "desc"
//       }
//     }
//   ],
//   "query": {
//     "bool": {
//       "must": [
//         {
//           "match": {
//             "searchvalue": {
//               "query": "kings sox",
//               "fuzziness": 4,
//               "lenient": "true",
//               "operator": "and"
//             }
//           }
//         }
//       ]
//     }
//   }
// }