/**
 * Ring buffer for limiting promise concurrency.
 *
 * Promises can be pushed into the buffer until it is full, after which
 * each additional push will await on the tail of the buffer.
 * 
 * You must call and await flush() after having pushed all your promises.
 * You cannot push or flush again after flushing the buffer.
 * 
 * Results of the promises pushed into the buffer can optionally be
 * collected for retrieval on flush().
 * 
 * Note that this class will never catch any errors from rejected promises,
 * you will have to handle errors and rejections yourself, for example by
 * wrapping promises passed to push(). Any unhandled rejections will cause
 * push() or flush() to blow up and leave the buffer instance probably unusable.
 */
export class PromiseRingAwaiter<T>
{
  _size: number;
  _buffer: Array<Promise<T>>
  _ptr: number;
  _results?: Array<T>
  _flushed: boolean;

  /**
   * Constructor.
   *
   * @param size : number, the size of the rung buffer
   * @param collectResults : boolean, whether to collect the results of promises pushed into the buffer
   */
  constructor(size: number, collectResults: boolean = false)
  {
    this._size = size;
    this._buffer = new Array(size);
    this._ptr = 0;
    if (collectResults)
    {
      this._results = [];
    }
    this._flushed = false;
  }

  /**
   * Push a promise into the buffer.
   * If the buffer is full, then await the promise at the tail of the buffer.
   *
   * @param p : Promise<T>, a promise to push into the buffer.
   */
  async push(p: Promise<T>)
  {
    if (this._flushed)
    {
      throw new Error('Trying to push in to an already flushed PromiseRingAwaiter. Please create a new instance.');
    }

    this._buffer[this._ptr] = p;

    const next = (this._ptr + 1) % this._size;
    if (this._buffer[next])
    {
      const result = await this._buffer[next];
      if (this._results)
      {
        this._results.push(result);
      }
    }
    this._ptr = next;
  }

  /**
   * Await all remaining promises in the buffer, in order of insertion.
   * If this instance used `collectResults` then return an array of all the values
   * returned by every promise pushed into the buffer, in order of insertion;
   * else return undefined.
   */
  async flush(): Promise<Array<T> | void>
  {
    if (this._flushed)
    {
      throw new Error('Trying to flush an already flushed PromiseRingAwaiter. Please create a new instance.');
    }

    this._flushed = true;

    for (let i = 0; i < this._size; ++i)
    {
      const p = (this._ptr + i + 1) % this._size;
      if (this._buffer[p])
      {
        const result = await this._buffer[p];
        if (this._results)
        {
          this._results.push(result);
        }
      }
    }

    if (this._results)
    {
      return this._results;
    }
  }
}
