import csvParse from 'csv-parse';
import fs from 'fs';
import eventStream from 'event-stream';

export type ChunkCallback = (lines: string[], number: number) => Promise<void>;

export async function parseCSVtoNDJSONinChunks(filename: string, chunksize: number, callback: ChunkCallback): Promise<void> {
  const lineCache: string[] = [];
  let chunkNum = 0;
  const parser = csvParse({ columns: true });

  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(filename)
      .pipe(parser)
      .pipe(eventStream.stringify())
      .pipe(eventStream.mapSync(async (line: string) => {
        lineCache.push(line);
        if (lineCache.length === chunksize) {
          // pause the readstream
          stream.pause();
          // process the chunk
          await callback(lineCache.splice(0, lineCache.length), chunkNum++);
          // resume the readstream
          stream.resume();
        }
      })
      .on('error', (err: Error) => {
        console.log('Error while reading file', filename, err);
        reject(err);
      })
      .on('end', async () => {
        if (lineCache.length > 0) {
          await callback(lineCache, chunkNum++);
        }
        console.log('Read entire file', filename);
        resolve();
      })
    );
  });
}

export async function readNDJSONinChunks(filename: string, chunksize: number, callback: ChunkCallback): Promise<void> {
  const lineCache: string[] = [];
  let chunkNum = 0;

  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(filename)
      .pipe(eventStream.split())
      .pipe(eventStream.mapSync(async (line: string) => {
        lineCache.push(line);
        if (lineCache.length === chunksize) {
          // pause the readstream
          stream.pause();
          // process the chunk
          await callback(lineCache.splice(0, lineCache.length), chunkNum++);
          // resume the readstream
          stream.resume();
        }
      })
      .on('error', (err: Error) => {
        console.log('Error while reading file', filename, err);
        reject(err);
      })
      .on('end', async () => {
        if (lineCache.length > 0) {
          await callback(lineCache, chunkNum++);
        }
        console.log('Read entire file', filename);
        resolve();
      })
    );
  });
}
